<?php
// $Id:

/**
 * @file
 * Contains theme override functions and preprocess functions for the theme.
 *
 * ABOUT THE TEMPLATE.PHP FILE
 *
 *   The template.php file is one of the most useful files when creating or
 *   modifying Drupal themes. You can add new regions for block content, modify
 *   or override Drupal's theme functions, intercept or make additional
 *   variables available to your theme, and create custom PHP logic. For more
 *   information, please visit the Theme Developer's Guide on Drupal.org:
 *   http://drupal.org/theme-guide
 *
 * OVERRIDING THEME FUNCTIONS
 *
 *   The Drupal theme system uses special theme functions to generate HTML
 *   output automatically. Often we wish to customize this HTML output. To do
 *   this, we have to override the theme function. You have to first find the
 *   theme function that generates the output, and then "catch" it and modify it
 *   here. The easiest way to do it is to copy the original function in its
 *   entirety and paste it here, changing the prefix from theme_ to bloggrail_.
 *   For example:
 *
 *     original: theme_breadcrumb()
 *     theme override: bloggrail_breadcrumb()
 *
 *   where bloggrail is the name of your sub-theme. For example, the
 *   zen_classic theme would define a zen_classic_breadcrumb() function.
 *
 *   If you would like to override any of the theme functions used in Zen core,
 *   you should first look at how Zen core implements those functions:
 *     theme_breadcrumbs()      in zen/template.php
 *     theme_menu_item_link()   in zen/template.php
 *     theme_menu_local_tasks() in zen/template.php
 *
 *   For more information, please visit the Theme Developer's Guide on
 *   Drupal.org: http://drupal.org/node/173880
 *
 * CREATE OR MODIFY VARIABLES FOR YOUR THEME
 *
 *   Each tpl.php template file has several variables which hold various pieces
 *   of content. You can modify those variables (or add new ones) before they
 *   are used in the template files by using preprocess functions.
 *
 *   This makes THEME_preprocess_HOOK() functions the most powerful functions
 *   available to themers.
 *
 *   It works by having one preprocess function for each template file or its
 *   derivatives (called template suggestions). For example:
 *     THEME_preprocess_page    alters the variables for page.tpl.php
 *     THEME_preprocess_node    alters the variables for node.tpl.php or
 *                              for node-forum.tpl.php
 *     THEME_preprocess_comment alters the variables for comment.tpl.php
 *     THEME_preprocess_block   alters the variables for block.tpl.php
 *
 *   For more information on preprocess functions and template suggestions,
 *   please visit the Theme Developer's Guide on Drupal.org:
 *   http://drupal.org/node/223440
 *   and http://drupal.org/node/190815#template-suggestions
 */


/**
 * Implementation of HOOK_theme().
 */
function bloggrail_theme(&$existing, $type, $theme, $path) {
  $hooks = zen_theme($existing, $type, $theme, $path);
  // Add your theme hooks like this:
  /*
  $hooks['hook_name_here'] = array( // Details go here );
  */
  // @TODO: Needs detailed comments. Patches welcome!
  return $hooks;
}

/**
 * Override or insert variables into all templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered (name of the .tpl.php file.)
 */
/* -- Delete this line if you want to use this function
function bloggrail_preprocess(&$vars, $hook) {
  $vars['sample_variable'] = t('Lorem ipsum.');
}
// */

/**
 * Override or insert variables into the page templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("page" in this case.)
 */
/* -- Delete this line if you want to use this function
function bloggrail_preprocess_page(&$vars, $hook) {
  $vars['sample_variable'] = t('Lorem ipsum.');
}
// */


/**
 * Override or insert variables into the node templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("node" in this case.)
 */
/* -- Delete this line if you want to use this function */
function bloggrail_preprocess_node(&$vars, $hook) {
    //This will add quick admin links such as edit and Delete along node display
    //Original code from http://11heaven.com
    // If we are in teaser view and have administer nodes permission
  if (user_access('administer nodes')) {
    // get the human-readable name for the content type of the node
    //$content_type_name = node_get_types('name', $vars['node']);
    // making a back-up of the old node links...
    $links = $vars['node']->links;
    $nid = $vars['node']->nid;
    $links['quick-edit'] = array(
      'title' => 'Edit ',
      'href' => 'node/' . $nid . '/edit',
      'query' => drupal_get_destination(),
    );
    // and then adding the quick delete link
    $links['quick-delete'] = array(
      'title' => 'Delete ',
      'href' => 'node/' . $nid . '/delete',
      'query' => drupal_get_destination(),
    );
	$vars['links'] = theme('links', $links, array('class' => 'links inline'));
  }
}


/**
 * Override or insert variables into the comment templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("comment" in this case.)
 */
/* -- Delete this line if you want to use this function
function bloggrail_preprocess_comment(&$vars, $hook) {
  $vars['sample_variable'] = t('Lorem ipsum.');
}
// */

/**
 * Override or insert variables into the block templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("block" in this case.)
 */
/* -- Delete this line if you want to use this function
function bloggrail_preprocess_block(&$vars, $hook) {
  $vars['sample_variable'] = t('Lorem ipsum.');
}
// */

/**
  * Customize the output of comment submission
*/
function bloggrail_comment_submitted($comment) {
  
  return t('<strong>!username</strong> | !datetime - !daterel',
    array(
      '!username' => theme('username', $comment),
      '!datetime' => format_date($comment->timestamp, 'custom', 'F j, Y'),
      '!daterel' =>  format_interval(time() - $comment->timestamp),
    ));
}

/**
  * Customize the output of node submission
*/
function bloggrail_node_submitted($node) {

  return t('<strong>!username</strong> | !datetime - !daterel',
    array(
      '!username' => theme('username', $node),
      '!datetime' => format_date($node->created, 'custom', 'F j, Y'),
      '!daterel' =>  format_interval(time() - $node->created),
    ));
}

/** 
  * Customize drupal rss image here and also the title text
  * You replace the image as you please, adjust the class accordingly
  * If you want to change the icon position, change it in your page.tpl.php
*/

function bloggrail_feed_icon($url, $title = NULL) {
  $title = t('Stay updated on the news');
  if ($image = theme('image', path_to_theme() . '/images/rss.png', t('Stay updated with the news'), $title)) {
    return '<a href="'. check_url($url) .'" class="feed-icon">'. $image .'</a>';
  }
}


/**
  * Theme override for theme_menu_item()
  * Add menu ID and custom Name based on menu titles for block menus, great for icons
  * If necessary this may replace primary links with block primary menu for advanced theming
*/
function bloggrail_menu_item($link, $has_children, $menu = '', $in_active_trail = FALSE, $extra_class = NULL) {
  $class = ($menu ? 'expanded' : ($has_children ? 'collapsed' : 'leaf'));
  if (!empty($extra_class)) {
    $class .= ' '. $extra_class;
  }
  if ($in_active_trail) {
    $class .= ' active-trail';
  }
 
  // Add unique identifier
  static $item_id = 0;
  $item_id += 1;
   $id .= 'mid-' . $item_id;
   // Add semi-unique class
  $class .= ' ' . preg_replace("/[^a-zA-Z0-9]/", "", strip_tags($link));
 
  return '<li class="'. $class .'" id="' . $id . '">'. $link . $menu ."</li>\n";
}

/**
  * If you have trouble to create a custom maintenance page, see http://drupal.org/node/195435
*/
function bloggrail_maintenance_page($content, $messages = TRUE, $partial = FALSE) {
require_once 'maintenance-page.tpl.php';
}